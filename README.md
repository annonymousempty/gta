# GTA CooP
GTA CooP is a multiplayer modification for Grand Theft Auto Five, that utilizes none of the online code from Rockstar.

# Downloads
Downloads are available [here](https://gtacoop.com/downloads).

# Information
If you have any questions feel free to join our discord [![join our discord](https://discordapp.com/api/guilds/445676008901705729/embed.png)](https://discord.gg/E6Tty42)

Original code © 2016 [Guad](https://github.com/Guad)

Modifications © 2016 [Bluscream](https://github.com/Bluscream), [wolfmitchell](https://github.com/soccermitchy)

Modifications © 2018 [TheIndra](https://github.com/TheIndra55)